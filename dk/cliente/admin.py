#-*- coding: utf-8 -*-
from django.contrib import admin
from dk.cliente.models import Cliente
#from dk.principal.admin import SiteInline

class ClienteAdmin(admin.ModelAdmin):
    #inlines = [SiteInline,]
    pass

admin.site.register(Cliente, ClienteAdmin)
