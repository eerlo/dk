#-*- coding: utf-8 -*-
from django.db import models
from dk.core.models import Model
from django.contrib.auth.models import User

class Cliente(Model):
    """
    Model que representa os dados de um cliente, vinculado a um User
    """
    user = models.OneToOneField(User)

    #Dados adicionais do cliente abaixo


    def __unicode__(self):
        return self.user.username
