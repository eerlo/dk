#-*- coding: utf-8 -*-

from django import forms
from dk.contato.models import Contato

class ContatoForm(forms.ModelForm):

    class Meta:
        model = Contato
        exclude = (u'cliente', u'email_enviado')

    def __init__(self, *args, **kwargs):
        super(ContatoForm, self).__init__(*args, **kwargs)
        msg = u'Informe o seu nome, para que possamos entrar em contato'
        msg += u' com você.'
        self.fields.get('nome').error_messages[u'required'] = msg

    def clean(self):
        telefone = self.cleaned_data.get(u'telefone')
        email = self.cleaned_data.get(u'email')
        if telefone == u'' and email == u'':
            msg = u'Informe o telefone ou e-mail para que possamos entrar' 
            msg += u' em contato como você.'
            raise forms.ValidationError(msg)
        return self.cleaned_data
