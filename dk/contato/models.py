#-*- coding: utf-8 -*-

from django.db import models

from dk.core.models import Model
from dk.cliente.models import Cliente

class Contato(Model):
    """
    Pagina de contado de um site.
    """
    
    class Meta:
        verbose_name = u'Contato'
        verbose_name_plural = u'Contatos'
 
    cliente = models.ForeignKey(
        Cliente,
        null=False,
        blank=False
    )

    nome = models.CharField(
        u'Nome',
        max_length=50,
        null=False,
        blank=False
    )
 
    telefone = models.CharField(
        u'Telefone',
        max_length=11,
        help_text='Ex.: 5412345678',
        null=True,
        blank=True
    )

    email = models.EmailField(
        u'E-mail',
        help_text='Ex.: exemplo@exemplo.com.br',
        null=True,
        blank=True
    )

    mensagem = models.TextField(
        u'Mensagem',
        max_length=1000,
        null=True,
        blank=True
    )

    email_enviado = models.BooleanField(
        'E-mail enviado com sucesso?',
        default=False,
    )

    def __unicode__(self):
        return unicode(self.nome)
