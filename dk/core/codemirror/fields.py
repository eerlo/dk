# -*- coding: utf-8 -*-
"""
Fields CodeMirror.
"""
from django.db import models
from django import forms
from dk.core.codemirror import widgets

class CodeMirrorHtmlField(models.TextField):
    """
    Classe de definição de um campo CodeMirror de model, específico para html,
    css e javascript. Possui todos as opções originais de um TextField.
    """
    def __init__(self, theme=None, *args, **kwargs):
        """
        Sobrescrita do init para a inclusão do parâmetro "theme", permitindo a
        passagem do tema a ser utilizado pelo campo no momento de sua criação
        em um model. Caso ele não seja informado, o tema será o definido na
        variável "CODEMIRROR_THEME" presente nos settings. Caso ela também
        não exista, o campo utilizará o tema "cobalt".

        Ex: campo_html_model = CodeMirrorHtmlField(theme=u'cobalt')
        """
        self.theme = theme
        super(CodeMirrorHtmlField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        """
        Altera o dicionário "defaults" do campo para utilização do campo
        CodeMirrorHtmlFormField ao ser exibido em um model e realiza a
        passagem do tema do campo.
        """
        defaults = {
            'form_class': CodeMirrorHtmlFormField,
        }

        defaults.update(kwargs)
        defaults.update({'theme': self.theme})

        return super(CodeMirrorHtmlField, self).formfield(**defaults)


class CodeMirrorHtmlFormField(forms.fields.Field):
    """
    Classe que define um campo CodeMirror de form. Utilizado
    pelo campo de model para exibição em tela.
    """
    def __init__(self, theme=None, *args, **kwargs):
        """
        Sobrescrita do init para passagem do parâmetro "theme" ao widget
        e definição do widget que será utilizado pelo campo do form.
        """
        codemirror = widgets.CodeMirrorHtmlTextarea(theme=theme)
        kwargs.update({'widget': codemirror})
        super(CodeMirrorHtmlFormField, self).__init__(*args, **kwargs)

