# -*- coding: utf-8 -*-
"""
Widgets para campos CodeMirror.
"""
from django import forms
from django.conf import settings
from django.utils.safestring import mark_safe

# Configurações default
CODEMIRROR_PATH = getattr(settings, 'CODEMIRROR_PATH', u'codemirror')

if CODEMIRROR_PATH.endswith('/'):
    CODEMIRROR_PATH = CODEMIRROR_PATH[:-1]

CODEMIRROR_THEME = getattr(settings, 'CODEMIRROR_THEME', u'cobalt')

class CodeMirrorHtmlTextarea(forms.Textarea):
    """
    Widget de campo Textarea para utilização do CodeMirror com base em campo
    Textarea de form.

    Obs.: Específico para o modo "HtmlMixed", mas o tema pode ser alterado.
    CodeMirror: http://codemirror.net/
    """
    @property
    def media(self):
        """
        Arquivos JS e CSS que deverão ser carregados.
        """
        return forms.Media(
            css = {'all': [
                        u'/static/%s/css/codemirror.css' % CODEMIRROR_PATH,
                        u'/static/%s/css/fullscreen.css' % CODEMIRROR_PATH,
                        u'/static/%s/css/folding.css' % CODEMIRROR_PATH,
                        u'/static/%s/css/%s.css' % (CODEMIRROR_PATH, self.theme)
                    ]
                },
            js = [
                u'/static/%s/js/codemirror.js' % CODEMIRROR_PATH,
                u'/static/%s/js/htmlmixed.js' % CODEMIRROR_PATH,
                u'/static/%s/js/foldcode.js' % CODEMIRROR_PATH,
                u'/static/%s/js/foldgutter.js' % CODEMIRROR_PATH,
                u'/static/%s/js/brace-fold.js' % CODEMIRROR_PATH,
                u'/static/%s/js/xml-fold.js' % CODEMIRROR_PATH,
                u'/static/%s/js/comment-fold.js' % CODEMIRROR_PATH,
                u'/static/%s/js/indent-fold.js' % CODEMIRROR_PATH,
                u'/static/%s/js/xml.js' % CODEMIRROR_PATH,
                u'/static/%s/js/fullscreen.js' % CODEMIRROR_PATH,
                u'/static/%s/js/active-line.js' % CODEMIRROR_PATH,
            ]
        )

    def __init__(self, attrs=None, theme=None, **kwargs):
        """
        Construtor do CodeMirrorHtmlTextarea. Adiciona os atributos tema e mode
        ao campo.

        Parâmetro "theme": nome do tema.
        """
        super(CodeMirrorHtmlTextarea, self).__init__(attrs=attrs, **kwargs)

        self.theme = theme or CODEMIRROR_THEME
        self.mode = u'htmlmixed'

    def get_codigo_configs_js(self, name):
        """
        Retorna o conteúdo do arquivo de configurações javascript do
        CodeMirrorHtmlTextarea após as alterações de nome do campo,
        modo (htmlmixed) e tema.
        """
        caminho_arquivo_js_configs = u'%s/%s%s' % (settings.DK_STATIC_ROOT,
                                                   CODEMIRROR_PATH,
                                                   u'/js/dk-codemirror.js')

        arquivo_js_configs = open(caminho_arquivo_js_configs,'r')
        conteudo_arquivo_js_configs = arquivo_js_configs.read()
        arquivo_js_configs.close()

        conteudo_arquivo_js_configs = conteudo_arquivo_js_configs.\
                                        replace(u'campo_codemirror', name)
        conteudo_arquivo_js_configs = conteudo_arquivo_js_configs.\
                                        replace(u'mode_', self.mode)
        conteudo_arquivo_js_configs = conteudo_arquivo_js_configs.\
                                        replace(u'theme_', self.theme)

        return u'<script type="text/javascript">%s</script>' % \
                                                    conteudo_arquivo_js_configs

    def render(self, name, value, attrs=None):
        """
        Renderiza o CodeMirrorHtmlTextarea com os atributos necessários.
        """
        codigo_configs_js = self.get_codigo_configs_js(name)

        output = [super(CodeMirrorHtmlTextarea, self).\
                                render(name, value, attrs),codigo_configs_js]

        return mark_safe('\n'.join(output))

