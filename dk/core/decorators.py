#-*- coding: utf-8 -*-
"""
Decorators do projeto.
"""
from django.http import Http404
from dk.principal.models import Site


def admin_required(view):
    """
    Decorator para ser utilizado sobre as views de administração dos sites.
    """
    def verifica_possui_administracao_site(request, *args, **kwargs):
        """
        Verifica se o site acessado possui administração disponível, se não
        possuir, uma página 404 deve ser exibida. Se possuir, deve retornar
        a view original.
        """
        if Site.objects.filter(dominio=request.get_host(),
                               possui_administracao=True).exists():
            return view(request, *args, **kwargs)
        raise Http404

    return verifica_possui_administracao_site

