#-*- coding: utf-8 -*-
from dk.principal.models import Site, Pagina
from django.http import HttpResponse, HttpResponseNotFound

PREFIXOS_IGNORAR = (u'/admin',
                    u'/administracao-do-site',
                  )

URLS_IGNORAR = (u'/favicon.ico',)

class SiteMiddleware(object):
    """
    Middleware que serve de proxy, redirecionando todos os requests que chegam
    para o processamento e retorno adequado da página HTML.
    """
    def process_request(self, request): 
        """
        Método executado antes de todos requests chegarem na view.
        """
        # pega a url após o domínio, com a barra inicial
        url = request.path

        # se a url não deve ser considerada no processamento, pois é uma
        # url especial, como por exemplo o favicon.ico
        if url in URLS_IGNORAR:
            return None

        # TODO: verificar depois se e necessario.
        for i in PREFIXOS_IGNORAR:
            if url.startswith(i):
                return None

        # pega o dominio digitado na url(com a porta, caso possua explicita)
        host = request.get_host()
        try:
            site = Site.objects.get(dominio=host)
        except Site.DoesNotExist:
            return HttpResponse(u'Site em construção.')

        # Pega a pagina do site em questao
        try:
            pagina = site.paginas.get(url=url)
        except Pagina.DoesNotExist:
            return HttpResponseNotFound(u'404 - Página não encontrada.')

        # chama o método da página que retorna o Response final.
        return HttpResponse(pagina.render_html(request))
