#-*- coding: utf-8 -*-
from django.db import models

class Model(models.Model):
    """
    Model base para ser herdados por todos do projeto dk.
    """
    criado_em = models.DateTimeField(auto_now_add=True)
    alterado_em = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

