#-*- coding: utf-8 -*-
import os
import shutil
from django.core.files import File
from dk.principal.models import Site, Pagina, ImagemSite, CssPagina, JsPagina
def extrai_zip(caminho_arquivo, site):
    id_cliente = site.cliente.pk
    nome_arquivo = caminho_arquivo.split('/')[-1]
    nome_arquivo_sem_zip = nome_arquivo.replace('.zip', '')
    caminho_diretorio_raiz = '/tmp/%s/%s/' % (id_cliente, nome_arquivo_sem_zip)
    #remove o diretorio caso exista
    os.system(u'rm -rf /tmp/%s/' % id_cliente)
    #cria ele novamente
    os.mkdir('/tmp/%s' % id_cliente)
    diretorio_temp = '/tmp/%s/dk_temp/' % id_cliente
    os.mkdir(diretorio_temp)
    #move o arquivo para o diretorio criado
    shutil.move(caminho_arquivo, '/tmp/%s/' % id_cliente)
    #extrai o arquivo dentro da pasta criada
    ok = os.system('unzip -o /tmp/%s/%s -d /tmp/%s/' % (id_cliente, nome_arquivo, id_cliente))
    if ok == 1:
        raise Exception('Erro ao extrair o arquivo.')
    #pega todos os arquivos/pastas extraidos
    arquivos = os.listdir('/tmp/%s/%s/' % (id_cliente, nome_arquivo_sem_zip))
    #pega todos os html, que estao no primeiro nivel da pasta
    htmls = [f for f in arquivos if f.endswith('.html')]

    #vai criar uma pagina para cada arquivo html
    for arq_html in htmls:
        nome_html_sem_extensao = arq_html.replace('.html', '')
        caminho_total = u'/tmp/%s/%s/%s' %(id_cliente, nome_arquivo_sem_zip, arq_html)
        new_pagina = Pagina()
        new_pagina.html = open(caminho_total, 'r').read()
        if nome_html_sem_extensao == 'index':
            new_pagina.url = '/'
        else:
            new_pagina.url = '/' + nome_html_sem_extensao
        print 'Cadastrando pagina %s...' % new_pagina.url 
        new_pagina.site = site
        new_pagina.save()
        print 'pagina %s ok!' % new_pagina.url

        #anda por todas as imagens da pagina, fazendo upload das mesmas,
        #e referenciando elas dentro do codigo html.
        pos1 = 0
        novos_imgs = []
        print 'Buscando imagens da pagina %s...' % new_pagina.url
        while pos1 > -1:
            pos1 = new_pagina.html.find('<img', pos1+1)
            if pos1 > -1:
                pos2 = new_pagina.html.find('/>', pos1) +2
                atual = new_pagina.html[pos1:pos2]
                pos_src_atual = atual.find('src=',0)
                src_atual = atual[pos_src_atual+5:atual.find('"', pos_src_atual+5)]
                identificador_img = src_atual.split('/')[-1].split('.')[0]
                novos_imgs.append({'atual': atual, 'novo': atual.replace(src_atual, '{{ imagens.%s }}' % identificador_img)})
                img_nova = ImagemSite(site=new_pagina.site)
                img_nova.identificador = identificador_img
                path_arquivo = '%s%s' % (caminho_diretorio_raiz, src_atual,)
                img_nova.arquivo = File(open(path_arquivo))
                img_nova.save()

        for i in novos_imgs:
            new_pagina.html = new_pagina.html.replace(i['atual'], i['novo'])
            new_pagina.save()
        print 'Imagens ok!'


        #Busca os arquivos css da pagina, faz upload dos mesmos, e cria a referencia
        #dos mesmos dentro do html
        CODIGO_CSS_PAGINA = '{% for i in css.pagina %}<link href="{{i}}" rel="stylesheet" type="text/css" media="all">{%endfor%}'
        print 'Buscando arquivos css da pagina %s' % new_pagina.url
        num_link_ignorados = 0
        for num_link, link in enumerate(new_pagina.html.split('<link')[1:]):
            link_completo = '<link' + link.split('>')[0] + '>'
            caracter_href = link_completo.split('href=')[1][0]
            href_link = link_completo.split('href=')[1][1:].split(caracter_href)[0]
            if 'http://' in href_link or 'https://' in href_link:
                num_link_ignorados += 1
                continue
            local_absoluto_css = '%s%s' % (caminho_diretorio_raiz, href_link)
            conteudo = open(local_absoluto_css, 'r').read()
            array = conteudo.split("{")
            regras_css = []
            for c,i in enumerate(array[1:]):
                regra = array[c].split("}")[-1]
                completo = regra + "{" + i.split("}")[0] + "}"
                regras_css.append(completo.strip())
            
            regras_css_deixar_inline = []
            for i in regras_css:
                if "url(" in i:
                    regras_css_deixar_inline.append(i)

            css_colocar_pagina = []
            for i in regras_css_deixar_inline:
                local_img = i.split("url(")[1].split(")")[0]
                identificador_img = local_img.split(".")[-2].split("/")[-1].replace('-', '_')
                regra_deixar_inline = i.replace(local_img, "{{ imagens.%s }}" % identificador_img)
                css_colocar_pagina.append(regra_deixar_inline)
                conteudo_regra = i.split("{")[1].split("}")[0]
                conteudo = conteudo.replace(conteudo_regra, "")
                local_absoluto_img = '%s%s/%s' % (caminho_diretorio_raiz, os.path.dirname(href_link), local_img)
                #cria imagem
                if os.path.exists(local_absoluto_img):
                    img = ImagemSite()
                    img.arquivo = File(open(local_absoluto_img))
                    img.site = new_pagina.site
                    img.identificador = identificador_img
                    img.save()
                else:
                    print 'Imagem nao existe no diretorio: %s' % local_absoluto_img
            local_arquivo_certo_temp = diretorio_temp + href_link.split('/')[-1]
            open(local_arquivo_certo_temp, 'w').write(conteudo)
            new_css = CssPagina()
            new_css.arquivo = File(open(local_arquivo_certo_temp))
            new_css.pagina = new_pagina
            new_css.save()
            str_colocar_pagina = '\n'.join(css_colocar_pagina)
            str_colocar_pagina = '<style type="text/css">' + str_colocar_pagina + '</style>'   
            if not css_colocar_pagina:
                str_colocar_pagina = ""
            if num_link - num_link_ignorados == 0:
                new_pagina.html = new_pagina.html.replace(link_completo, CODIGO_CSS_PAGINA + '\n' + str_colocar_pagina)
            else:
                new_pagina.html = new_pagina.html.replace(link_completo, str_colocar_pagina)
            new_pagina.save()

        # Busca os Javascripts
        scripts = new_pagina.html.split('<script')
        quantos_scripts_cima = 0
        quantos_scripts_baixo = 0
        CODIGO_JS_PAGINA_TOP = '{% for i in js.pagina.top %}<script type="text/javascript" src="{{ i }}"></script>{%endfor%}'
        CODIGO_JS_PAGINA_BOTTOM = '{% for i in js.pagina.bottom %}<script type="text/javascript" src="{{ i }}"></script>{%endfor%}'
        for i in scripts[1:]:
            conteudo_script = i.split('</script>')[0]
            #verifica se eh um script externo
            if 'src=' in conteudo_script:
                caracter_src = conteudo_script.split('src=')[1][0]
                src_script = conteudo_script.split('src='+caracter_src)[1].split(caracter_src)[0]
                caminho_absoluto_script = os.path.join(caminho_diretorio_raiz+src_script)
                script_completo = '<script' + conteudo_script + '</script>'
                local = 'cima' if new_pagina.html.find('<body') > new_pagina.html.find(script_completo) else 'baixo'
                js_novo = JsPagina()
                js_novo.pagina = new_pagina
                js_novo.local = 1 if local == 'cima' else 2
                js_novo.arquivo = File(open(caminho_absoluto_script))
                js_novo.save()
                if local == 'cima':
                    if quantos_scripts_cima == 0:
                        new_pagina.html = new_pagina.html.replace(script_completo, CODIGO_JS_PAGINA_TOP)
                    else:
                        new_pagina.html = new_pagina.html.replace(script_completo, "")
                    new_pagina.save()
                    quantos_scripts_cima += 1
                if local == 'baixo':
                    if quantos_scripts_baixo == 0:
                        new_pagina.html = new_pagina.html.replace(script_completo, CODIGO_JS_PAGINA_BOTTOM)
                    else:
                        new_pagina.html = new_pagina.html.replace(script_completo, "")
                    new_pagina.save()
                    quantos_scripts_baixo += 1
        print 'Js ok!'
