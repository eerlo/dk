#-*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.filter
def get_static_absolute_url(value):
    return '/static/%s' % value.split('/static/')[-1]
