#-*- coding: utf-8 -*-
from django.conf import settings
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from dk.principal.models import Site, Pagina, Pagina404, Pagina500,\
                                    PaginaContato

URLS_IGNORAR = (u'/favicon.ico',)

def mostra_pagina_view(request):
    """
    View principal, onde, entra caso nenhuma url seja encontrada pelo django.
    Esta view possui toda a lógica para mostrar as páginas.
    """
    # pega a url após o domínio, com a barra inicial
    url = request.path
 
    # se a url não deve ser considerada no processamento, pois é uma
    # url especial, como por exemplo o favicon.ico
    if url in URLS_IGNORAR:
        return None
 
    # pega o dominio digitado na url(com a porta, caso possua explicita)
    host = request.get_host()
    try:
        site = Site.objects.get(dominio=host)
    except Site.DoesNotExist:
        return HttpResponse(u'Site em construção.')
 
    # Pega a pagina do site em questao
    try:
        pagina = site.paginas.get(url=url)
        if hasattr(pagina, u'paginacontato'):
            pagina = pagina.paginacontato
        elif hasattr(pagina, u'paginabusca'):
            pagina = pagina.paginabusca
    except Pagina.DoesNotExist:
        raise Http404
 
    # chama o método da página que retorna o Response final.
    return HttpResponse(pagina.render_html(request))

def erro404(request):
    """
    Renderiza a página de erro 404 específica do site se ela existir ou
    uma página padrão com informações dinâmicas de acordo com o site.
    """
    dominio = request.get_host()
    site = Site.objects.get(dominio=dominio)

    if Pagina404.objects.filter(site=site).exists():
        pagina404 = Pagina404.objects.get(site=site)
        return HttpResponse(pagina404.render_html(request))

    try:
        nome_site = site.nome
    except Site.DoesNotExist:
        nome_site = None

    if PaginaContato.objects.filter(site=site).exists():
        site_contato_url = PaginaContato.objects.get(site=site).url
    else:
        site_contato_url = None

    return render_to_response('404.html',
                              {'nome_site': nome_site,
                               'site_url': dominio,
                               'contato_url': site_contato_url},
                              context_instance=RequestContext(request)
                        )

def erro500(request):
    """
    Renderiza a página de erro 500 específica do site se ela existir ou
    uma página padrão com informações dinâmicas de acordo com o site.
    """
    dominio = request.get_host()
    site = Site.objects.get(dominio=dominio)

    if Pagina500.objects.filter(site=site).exists():
        pagina500 = Pagina500.objects.get(site=site)
        return HttpResponse(pagina500.render_html(request))

    try:
        nome_site = site.nome
    except Site.DoesNotExist:
        nome_site = None

    return render_to_response('500.html',
                              {'nome_site': nome_site,
                               'site_url': dominio,
                               'email_contato': settings.EMAIL_HOST_USER},
                              context_instance=RequestContext(request)
                        )

