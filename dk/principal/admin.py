#-*- coding: utf-8 -*-

#imports django
from django.db import models
from django.contrib import admin

#imports dk
from dk.principal.models import (Site, Pagina, ConteudoPagina,
    ImagemSite, JsPagina, CssPagina, JsSite, CssSite, PaginaContato,
    PaginaBusca, AtributoItemPagina, ItemPagina, AtributoItemPagina,
    InstanciaItemPagina, ValorAtributoInstanciaItemPagina, Pagina404,
    Pagina500)
from dk.principal.forms import PaginaBuscaFormAdmin

VALOR_EXTRA = 1
                                    
### Início dos Inlines
class SiteInline(admin.TabularInline):
    model = Site
    extra = VALOR_EXTRA

class PaginaInline(admin.TabularInline):
    model = Pagina
    extra = VALOR_EXTRA

class ImagemSiteInline(admin.TabularInline):
    model = ImagemSite
    extra = VALOR_EXTRA

class JsSiteInline(admin.TabularInline):
    model = JsSite
    extra = VALOR_EXTRA

class CssSiteInline(admin.TabularInline):
    model = CssSite
    extra = VALOR_EXTRA

class ConteudoPaginaInline(admin.TabularInline):
    model = ConteudoPagina
    extra = VALOR_EXTRA

class JsPaginaInline(admin.TabularInline):
    model = JsPagina
    extra = VALOR_EXTRA

class CssPaginaInline(admin.TabularInline):
    model = CssPagina
    extra = VALOR_EXTRA
### Fim dos Inlines


### Início dos Admins
class SiteAdmin(admin.ModelAdmin):
    model = Site
    inlines = [PaginaInline, ImagemSiteInline, CssSiteInline, JsSiteInline]

class PaginaAdmin(admin.ModelAdmin):
    model = Pagina
    inlines = [ConteudoPaginaInline,
               JsPaginaInline, CssPaginaInline]

class ConteudoPaginaAdmin(admin.ModelAdmin):
    model = ConteudoPagina

class ImagemSiteAdmin(admin.ModelAdmin):
    model = ImagemSite

class JsPaginaAdmin(admin.ModelAdmin):
    model = JsPagina

class CssPaginaAdmin(admin.ModelAdmin):
    model = CssPagina

class JsSiteAdmin(admin.ModelAdmin):
    model = JsSite

class CssSiteAdmin(admin.ModelAdmin):
    model = CssSite

class PaginaContatoAdmin(admin.ModelAdmin):
    model = PaginaContato
    inlines = [ConteudoPaginaInline,
               JsPaginaInline, CssPaginaInline]

class PaginaBuscaAdmin(admin.ModelAdmin):
    form = PaginaBuscaFormAdmin
    inlines = [ConteudoPaginaInline,
               JsPaginaInline, CssPaginaInline]

class ValorAtributoInstanciaItemPaginaInline(admin.TabularInline):
    model = ValorAtributoInstanciaItemPagina

class ValorAtributoInstanciaItemPaginaAdmin(admin.TabularInline):
    model = ValorAtributoInstanciaItemPagina

class AtributoItemPaginaInline(admin.TabularInline):
    model = AtributoItemPagina

class AtributoItemPaginaAdmin(admin.ModelAdmin):
    model = AtributoItemPagina
    inlines = [ValorAtributoInstanciaItemPaginaInline,]

class InstanciaItemPaginaInline(admin.TabularInline):
    model = InstanciaItemPagina

class InstanciaItemPaginaAdmin(admin.ModelAdmin):
    model = InstanciaItemPagina
    inlines =[ValorAtributoInstanciaItemPaginaInline,]

class ItemPaginaInline(admin.TabularInline):
    model = ItemPagina

class ItemPaginaAdmin(admin.ModelAdmin):
    model = ItemPagina
    inlines = [AtributoItemPaginaInline,]

class Pagina404Admin(admin.ModelAdmin):
    """
    Admin do model de página 404.
    """
    model = Pagina404
    inlines = [
        ConteudoPaginaInline,
        JsPaginaInline,
        CssPaginaInline
    ]

class Pagina500Admin(admin.ModelAdmin):
    """
    Admin do model de página 500.
    """
    model = Pagina500
    inlines = [
        ConteudoPaginaInline,
        JsPaginaInline,
        CssPaginaInline
    ]

### Fim dos Admins

###register
admin.site.register(PaginaBusca, PaginaBuscaAdmin)
admin.site.register(PaginaContato, PaginaContatoAdmin)
admin.site.register(Site, SiteAdmin)
admin.site.register(Pagina, PaginaAdmin)
admin.site.register(ConteudoPagina, ConteudoPaginaAdmin)
admin.site.register(ImagemSite, ImagemSiteAdmin)
admin.site.register(CssPagina, CssPaginaAdmin)
admin.site.register(JsPagina, JsPaginaAdmin)
admin.site.register(JsSite, JsSiteAdmin)
admin.site.register(CssSite, CssSiteAdmin)
admin.site.register(ItemPagina, ItemPaginaAdmin)
admin.site.register(AtributoItemPagina, AtributoItemPaginaAdmin)
admin.site.register(InstanciaItemPagina, InstanciaItemPaginaAdmin)
admin.site.register(ValorAtributoInstanciaItemPagina)
admin.site.register(Pagina404, Pagina404Admin)
admin.site.register(Pagina500, Pagina500Admin)
