#-*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, \
                                      SetPasswordForm
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import get_current_site
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.http import int_to_base36
from django.template import loader

from dk.principal.models import PaginaBusca
from dk.principal.models import AtributoItemPagina

class AutenticacaoForm(AuthenticationForm):
    """
    Form com novos atributos dos campos do form de autenticação do django.
    """
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Usuário',
                'autofocus': 'autofocus',
                'class': 'form-control'
            }
        )
    )

    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Senha',
                'class' : 'form-control'
            }
        )
    )

class RecuperacaoSenhaForm(PasswordResetForm):
    """
    Form com novos atributos do campo email do form de recuperação de senha do
    django.
    """
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Informe seu e-mail',
                'autofocus': 'autofocus',
                'class': 'form-control'
            }
        )
    )

    def clean_email(self):
        """
        Sobrescrita do clean do campo e-mail para que os erros referentes a
        existência de uma conta com o e-mail informado não fossem lançados.
        Dessa forma, somente os erros relacionados ao dado informado são
        exibidos. Ex: "Este campo é obrigatório" ou "Informe um endereço
        válido".
        """
        UserModel = get_user_model()
        email = self.cleaned_data["email"]
        self.users_cache = UserModel.\
                                    _default_manager.filter(email__iexact=email)
        return email

    def save(self,
             nome_template_email,
             nome_template_assunto,
             usa_https=False,
             dominio=None,
             nome_site=None,
             remetente_email=None):
        """
        Gera um link (única utilização) de redefinição de senha e envia por
        e-mail ao usuário.
        """
        from django.core.mail import send_mail

        for usuario in self.users_cache:
            contexto = {
                'email': usuario.email,
                'dominio': dominio,
                'nome_site': nome_site,
                'uid': int_to_base36(usuario.pk),
                'usuario': usuario,
                'token': default_token_generator.make_token(usuario),
                'protocolo': usa_https and 'https' or 'http',
            }
            assunto = loader.render_to_string(nome_template_assunto, contexto)
            assunto = ''.join(assunto.splitlines())
            email = loader.render_to_string(nome_template_email, contexto)

            send_mail(assunto, email, remetente_email, [usuario.email])

class TrocaSenhaForm(SetPasswordForm):
    """
    Form com novos atributos dos campos do form de troca de senha do django.
    """
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Informe sua nova senha',
                'autofocus': 'autofocus',
                'class': 'form-control',
            }
        )
    )

    new_password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Informe sua nova senha novamente',
                'class': 'form-control'
            }
        )
    )


# Form utilizado no admin
class PaginaBuscaFormAdmin(forms.ModelForm):
    """
    Form adaptado para utilizar widget fe multipla escolha no admin
    """
    atributos_busca = forms.ModelMultipleChoiceField(
         queryset=AtributoItemPagina.objects.all(),
         widget=FilteredSelectMultiple(u'Atributos', is_stacked=False),
    )

    class Meta:
        model = PaginaBusca
    
    def clean(self):
        #Site que está selecionado
        site = self.cleaned_data.get(u'site')
        #Atributos selecionados.
        atributos_busca = self.cleaned_data.get(u'atributos_busca')
        for atributo in atributos_busca:
            if atributo.item_pagina.pagina.site != site:
                raise forms.ValidationError(
                    u'Alguns atributos selecionados para busca não são do \
                    site: %s.' % site)
        return self.cleaned_data
