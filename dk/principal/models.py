#-*- coding: utf-8 -*-

#imports django
from django.db import models
from django.template import Context, Template
from django.template.loader import render_to_string
from django.conf import settings
from django.core.mail import mail_admins
from django.core.paginator import Paginator
from django.contrib import messages
from django import forms

#imports do redactor
from redactor.fields import RedactorField
from redactor.widgets import RedactorEditor

#imports dk
from dk.core.codemirror.fields import CodeMirrorHtmlField
from dk.core.models import Model
from dk.core.utils import send_mail
from dk.cliente.models import Cliente
from dk.contato.forms import ContatoForm
from dk.principal.widgets import AdminImageWidget

class Site(Model):
    """
    Site de um cliente.
    """
    cliente = models.ForeignKey(Cliente)
    dominio = models.CharField(max_length=100)
    nome = models.CharField(max_length=100)
    possui_administracao = models.BooleanField(default=False)
    data_expiracao = models.DateField()

    class Meta:
        unique_together = (u"cliente", u"dominio")

    def __unicode__(self):
        return self.dominio


class Pagina(Model):
    """
    Pagina de um site.
    """
    site = models.ForeignKey(
        Site,
        related_name=u'paginas',
        null=False,
        blank=False
    )
    nome = models.CharField(
        max_length=50,
        null=False,
        blank=False
    )
    url = models.CharField(
        max_length=300,
        null=False,
        blank=False
    )
    html = CodeMirrorHtmlField(
        null=False,
        blank=False
    )

    class Meta:
        unique_together = (u"site", u"url")

    def __unicode__(self):
        return '%s - %s' % (self.site, self.url)

    def processa_request(self, request):
        return self.html , {}

    def render_html(self, request):
        #Busca o html do banco de dados
        html, contexto = self.processa_request(request)
        t = Template(html)

        #Joga os conteudos simples no contexto
        contexto.update({i.nome: i.conteudo for i in self.conteudos.all()})

        #Joga as imagens no contexto
        contexto.update(
           {u'imagens':
           {i.identificador: i.arquivo.url for i in self.site.imagemsite_set.all()}
        })

        #Joga os javascripts no contexto
        js = {'site': {}, 'pagina': {}}
        js['pagina']['bottom'] = [i.arquivo.url for i in self.jspagina_set.filter(local=2).order_by(u'pk')]
        js['pagina']['top'] = [i.arquivo.url for i in self.jspagina_set.filter(local=1).order_by(u'pk')]
        js['site']['bottom'] = [i.arquivo.url for i in self.site.jssite_set.filter(local=2).order_by(u'pk')]
        js['site']['top'] = [i.arquivo.url for i in self.site.jssite_set.filter(local=1).order_by(u'pk')]
        contexto.update({'js': js})

        #Joga os css no contexto
        css = {'site': [], 'pagina': []}
        css['pagina'] = [i.arquivo.url for i in self.csspagina_set.all().order_by(u'pk')]
        css['site'] = [i.arquivo.url for i in self.site.csssite_set.all().order_by(u'pk')]
        contexto.update({'css': css})

        #Tenta pegar a pagina atual ou a primeira
        try:
            pagina_atual = request.GET.get('pg_atual', 1)
        except:
            pagina_atual = 1

        #Joga os itens da pagina no contexto, PS: eu sou foda, hein!
        itens_pagina = {}
        for i in self.itempagina_set.all():
            itens_pagina[i.identificador] = []
            for j in i.instanciaitempagina_set.all():
                instancia_atual = {}
                for k in j.valoratributoinstanciaitempagina_set.all():
                    instancia_atual[k.atributo_item_pagina.identificador] = k.valor
                itens_pagina[i.identificador].append(instancia_atual)

            #Cria o Paginator para fazer paginação
            if i.quantos_por_pagina == 0:
                qts_por_pagina = len(itens_pagina[i.identificador])
            else:
                qts_por_pagina = i.quantos_por_pagina
            paginas = Paginator(itens_pagina[i.identificador], qts_por_pagina)
            if pagina_atual > paginas.num_pages:
                pagina_atual = paginas.num_pages
            itens_pagina[i.identificador] = paginas.page(pagina_atual)


        contexto['itens_pagina'] = itens_pagina
        c = Context(contexto)
        return t.render(c)

class PaginaContato(Pagina):
    """
    Pagina de contado de um site.
    """
    def processa_request(self, request):
        if request.method == u'POST':
            form = ContatoForm(request.POST)
            if form.is_valid():
                #Informações do cliente
                site = Site.objects.get(dominio=request.get_host())
                email_cliente = site.cliente.user.email

                #Informações do contato
                titulo = u'PEDIDO DE CONTATO PELO SEU SITE'

                #Teste para mensagem
                mensagem = form.cleaned_data.get(u'mensagem')
                if not mensagem:
                    mensagem = u'NÃO INFORMADA'

                #Teste para email
                email_contato = form.cleaned_data.get(u'email')
                email_p_contato = email_contato
                if not email_contato:
                    email_contato = u'NÃO INFORMADO'
                    email_p_contato = settings.CONTATOS_COM_CLIENTES

                #Teste para telefone
                telefone = form.cleaned_data.get(u'telefone')
                if not telefone:
                    telefone = u'NÃO INFORMADO'

                dicionario = {
                    u'nome': form.cleaned_data.get(u'nome'),
                    u'telefone': telefone,
                    u'email': email_contato,
                    u'mensagem': mensagem,
                }
                mensagem = render_to_string(
                    u'emails/email_contato.html',
                    dicionario,
                )

                try:
                    #Save
                    form.instance.cliente = site.cliente
                    form.save()
                    #Envia o email
                    send_mail(
                        subject=titulo,
                        body=mensagem,
                        from_email=email_p_contato,
                        recipient_list=(email_cliente,),
                        html=mensagem,
                    )
                    #Só troca para True se enviar o email com sucesso
                    form.instance.email_enviado = True
                    form.save()
                except Exception, ex:
                    titulo = u'ERRO AO ENVIAR E-MAIL DE CONTATO NO SITE'
                    mail_admins(titulo, ex)

                mensagem = u'Formulário enviado com sucesso.'
                messages.success(request, mensagem)
            else:
                mensagem = u'Ocorreu algum erro no preenchimento do formulário.'
                mensagem += u' Verifique e envie novamente.'
                messages.error(request, mensagem)
        else:
            msg = u''
            if u'msg' in request.GET:
                msg = request.GET.getlist(u'msg')[0]
            form = ContatoForm(initial={u'mensagem': msg})
        return self.html, {u'form': form}


CHOICES_TIPO_CONTEUDO = ((1, 'Campo Html',),
                         (2, 'Campo Normal Pequeno',),
                         (3, 'Campo Normal Grande',),)
DICT_TIPO_CONTEUDO = {1: {u'campo': forms.CharField, u'widget': RedactorEditor,},
                      2: {u'campo': forms.CharField, u'widget': forms.TextInput,},
                      3: {u'campo': forms.CharField, u'widget': forms.Textarea,},
                      }

class ConteudoPagina(Model):
    """
    Conteudo de uma pagina.
    """
    pagina = models.ForeignKey(Pagina, related_name=u'conteudos')
    nome = models.CharField(max_length=100)
    descricao = models.CharField(max_length=200, blank=True, null=True)
    conteudo = RedactorField()
    tipo_conteudo = models.IntegerField(choices=CHOICES_TIPO_CONTEUDO)

    def __unicode__(self):
        return u'%s - %s' % (self.pagina.url, self.descricao)

    class Meta:
        unique_together = (u"pagina", u"nome")


class StaticSite(Model):
    """
    Classe que representa um arquivo estatico global para todo o site.
    """
    site = models.ForeignKey(Site)
    arquivo = models.FileField(upload_to=u"media")

    class Meta:
        abstract = True

class StaticPagina(Model):
    """
    Classe que representa um arquivo estatico da pagina html.
    """
    pagina = models.ForeignKey(Pagina)
    arquivo = models.FileField(upload_to=u"media")

    class Meta:
        abstract = True

class ImagemSite(StaticSite):
    """
    Imagem estatica na pagina.
    """
    identificador = models.CharField(max_length=30)

    def __unicode__(self):
        return self.identificador

class JsPagina(StaticPagina):
    """
    Arquivo javascript da pagina.
    """
    local = models.IntegerField(choices=((1, u"Top"),(2, u"Bottom"),))

    def __unicode__(self):
        return u'%s - %s' % (self.arquivo, self.local)

class CssPagina(StaticPagina):
    """
    Arquivo css da pagina.
    """
    pass

    def __unicode__(self):
        return u'%s' % (self.arquivo)

class JsSite(StaticSite):
    """
    Arquivo javascript da pagina.
    """
    local = models.IntegerField(choices=((1, u"Top"),(2, u"Bottom"),))

    def __unicode__(self):
        return u'%s - %s' % (self.arquivo, self.local)

class CssSite(StaticSite):
    """
    Arquivo css da pagina.
    """
    pass

    def __unicode__(self):
        return u'%s' % (self.arquivo)


class ItemPagina(Model):
    """
    Classe que representa APENAS A ESTRUTURA de um item de uma pagina.
    """
    pagina = models.ForeignKey(Pagina)
    nome_plural = models.CharField(max_length=30,
            help_text=u"Nome que o usuário irá ver para entender o que está cadastrando")
    identificador = models.CharField(max_length=30, 
            help_text=u"Nome que a lista com estes itens terá no template")
    quantos_por_pagina = models.PositiveIntegerField(
            u"Quantidade de registros por pagina (0 - zero para ter sempre apenas 1 pagina).")

    class Meta:
        unique_together = (u"pagina", u"identificador")

    def __unicode__(self):
        return '%s da pagina %s do site %s' % (self.nome_plural, self.pagina.url, self.pagina.site.dominio)

TIPOS_ATRIBUTOS = (
    (1, u'Caracter',),
    (2, u'Texto Longo',),
    (3, u'Redactor',),
    (4, u'Inteiro',),
    (5, u'Decimal',),
    (6, u'Imagem',),
)

DICT_TIPOS_ATRIBUTOS = {
    1: {u'campo': forms.CharField, u'widget': forms.TextInput,},
    2: {u'campo': forms.CharField, u'widget': forms.Textarea,},
    3: {u'campo': forms.CharField, u'widget': RedactorEditor,},
    4: {u'campo': forms.IntegerField, u'widget': forms.TextInput,},
    5: {u'campo': forms.DecimalField, u'widget': forms.TextInput,},
    6: {u'campo': forms.ImageField, u'widget': AdminImageWidget,},
}

class AtributoItemPagina(Model):
    """
    Classe que representa APENAS A ESTRUTURA de um atributo
    de um item de uma pagina.
    """
    nome = models.CharField(max_length=30,
            help_text=u"Nome que o usuário vê")
    identificador = models.CharField(max_length=30,
            help_text=u"Nome que sera usado dentro dos templates.")
    tipo = models.IntegerField(choices=TIPOS_ATRIBUTOS)
    obrigatorio = models.BooleanField(default=True)
    item_pagina = models.ForeignKey(ItemPagina)

    def __unicode__(self):
        return '%s de %s' % (self.nome, self.item_pagina)


class PaginaBusca(Pagina):
    """
    Classe que vai realizar a busca na página.
    """
    def processa_request(self, request):
        resultados = None
        if request.method == u'GET':
            busca = request.GET.get(u'busca')
            if busca:
                resultados = InstanciaItemPagina.objects.filter(
                    valoratributoinstanciaitempagina__valor__icontains=busca,
                    valoratributoinstanciaitempagina__atributo_item_pagina__in=\
                        self.atributos_busca.all())
        return self.html, {u'resultados': resultados}

class Pagina404(Pagina):
    """
    Model da página de erro 404.
    """
    pass

class Pagina500(Pagina):
    """
    Model da página de erro 500.
    """
    pass

class InstanciaItemPagina(Model):
    """
    Classe que representa uma instância de um item de uma página, ou seja,
    uma instância deste model é o espelho de uma instância de um item de uma
    página.
    Possui atributos dinâmicos apenas em memória.
    """
    item_pagina = models.ForeignKey(ItemPagina)

    def __unicode__(self):
        return 'Instancia %s do item %s' % (self.pk, self.item_pagina)

class ValorAtributoInstanciaItemPagina(Model):
    """
    Classe que representa um valor de um atributo de um item de uma página,
    ou seja, uma instância deste model é o espelho de um atributo de um item de
    uma página.
    """
    instancia_item_pagina = models.ForeignKey(InstanciaItemPagina)
    atributo_item_pagina = models.ForeignKey(AtributoItemPagina)
    valor = models.TextField(null=True, blank=True)

    class Meta:
        unique_together = (u"instancia_item_pagina", u"atributo_item_pagina")
