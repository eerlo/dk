#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',

        url(r'^$', 'dk.principal.views.administrar_site'),
        url(r'paginas/(?P<pk_pagina>\d+)/$', 'dk.principal.views.form_pagina'),
        url(r'paginas/(?P<pk_pagina>\d+)/itens/(?P<pk_itempagina>\d+)/$', 'dk.principal.views.itens_pagina'),
        url(r'paginas/(?P<pk_pagina>\d+)/itens/(?P<pk_itempagina>\d+)/novo/$', 'dk.principal.views.form_itempagina_novo'),
        url(r'paginas/(?P<pk_pagina>\d+)/itens/(?P<pk_itempagina>\d+)/(?P<pk_instancia>\d+)/$', 'dk.principal.views.form_itempagina_alterar'),
        url(r'paginas/(?P<pk_pagina>\d+)/itens/(?P<pk_itempagina>\d+)/(?P<pk_instancia>\d+)/apagar/$', 'dk.principal.views.itempagina_apagar_instancia'),

)
