#-*- coding: utf-8 -*-
import os
import itertools
from django import forms
from django.utils.translation import ugettext as _
from django.conf import settings
from django.db.models import Count

from dk.core.utils import send_mail
from dk.principal.models import DICT_TIPO_CONTEUDO, CHOICES_TIPO_CONTEUDO,\
                                    DICT_TIPOS_ATRIBUTOS, Site


def get_dados_padrao_views_adm(request, pk_pagina):
    """
    Retorna alguns dados que a maioria das views de administração precisam,
    evitando assim a duplicação de código.
    """
    site = Site.objects.get(dominio=request.get_host())
    pagina = site.paginas.get(pk=pk_pagina)
    paginas = site.paginas.all().annotate(quantos_itens=Count(u'itempagina'))
    return site, pagina, paginas

def get_form_conteudos(pagina):
    class ConteudosPaginaForm(forms.Form):
        pass

    for i in pagina.conteudos.all():
        ConteudosPaginaForm.base_fields['%s' %i.nome] = DICT_TIPO_CONTEUDO[i.tipo_conteudo][u'campo'](widget= DICT_TIPO_CONTEUDO[i.tipo_conteudo][u'widget']())
        ConteudosPaginaForm.base_fields['%s' %i.nome].label = i.descricao

    return ConteudosPaginaForm 

def get_form_item_pagina(item_pagina):
    class ItemPaginaForm(forms.Form):
        pass
    for attr in item_pagina.atributoitempagina_set.all():
        campo = DICT_TIPOS_ATRIBUTOS[attr.tipo][u'campo']
        ItemPaginaForm.base_fields[attr.identificador] = campo(widget=DICT_TIPOS_ATRIBUTOS[attr.tipo][u'widget']())
        ItemPaginaForm.base_fields[attr.identificador].required = attr.obrigatorio

    return ItemPaginaForm

def get_available_name(name):
    """
    Returns a filename that's free on the target storage system, and
    available for new content to be written to.
    """
    dir_name, file_name = os.path.split(name)
    file_root, file_ext = os.path.splitext(file_name)
    # If the filename already exists, add an underscore and a number (before
    # the file extension, if one exists) to the filename until the generated
    # filename doesn't exist.
    count = itertools.count(1)
    while os.path.exists(name):
        # file_ext includes the dot.
        name = os.path.join(dir_name, "%s_%s%s" % (file_root, next(count), file_ext))
    return name

def trata_valor(attr_item_pagina, valor):
    if DICT_TIPOS_ATRIBUTOS[attr_item_pagina.atributo_item_pagina.tipo]['campo'] == forms.ImageField:
        # se nao tem valor, é pq nao eh obrigatorio, senao o form teria validado
        # então mantém o valor que já tinha, pois o user nao quis trocar
        if valor:
            pk_site = attr_item_pagina.atributo_item_pagina.item_pagina.pagina.site.pk
            local_imgs_cli = os.path.join(settings.ARQUIVOS_UPLOAD_CLIENTES_ROOT, str(pk_site))
            if not os.path.exists(local_imgs_cli):
                os.mkdir(local_imgs_cli)
            nome_unico = get_available_name(os.path.join(local_imgs_cli, valor.name))
            open(nome_unico, u'wb').write(valor.file.read())
            return nome_unico
        else:
            return attr_item_pagina.valor
    else:
        return valor

