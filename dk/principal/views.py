#-*- coding: utf-8 -*-

import datetime
#imports do django
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, login as django_login, \
                                logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.core.cache import cache
from django.core.files import File
from django.forms import ImageField
from django.http import Http404
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.template import RequestContext
from django.utils.http import base36_to_int
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

#import do dk
from dk.core.decorators import admin_required
from dk.principal.forms import AutenticacaoForm, RecuperacaoSenhaForm, \
                                   TrocaSenhaForm
from dk.principal.models import InstanciaItemPagina, \
                                    ValorAtributoInstanciaItemPagina, \
                                    Site
from dk.principal.utils import get_form_item_pagina, get_form_conteudos, \
                                   get_dados_padrao_views_adm, trata_valor


def verifica_tentativas_login_cache(request, form, valor_cache, chave_cache):
    """
    Verifica as tentativas de login por usuário para armazenamento em cache.

    O número máximo de tentativas e o tempo em minutos de bloqueio do login
    são definidos nos settings. O padrão é 5 tentativas e 15 minutos de
    bloqueio. Se o tempo de bloqueio for alterado, a mensagem "Você atingiu
    o limite máximo..." também deve ser alterada.
    """
    tentativas = valor_cache[0]
    data_hora_ultima_tentativa = valor_cache[1]
    tentativas += 1
    cache.set(chave_cache, [tentativas, data_hora_ultima_tentativa])

    if tentativas == settings.MAXIMO_TENTATIVAS_LOGIN:
        messages.warning(request, u'Você atingiu o limite máximo de'
                         u' tentativas de login. Uma nova tentativa'
                         u' de autenticação só será possível dentro'
                         u' de %i minutos.' % settings.MINUTOS_BLOQUEIO_LOGIN)
        form.errors.clear()
        data_hora_ultima_tentativa = datetime.datetime.now()
        request.method = "GET"

    elif tentativas > settings.MAXIMO_TENTATIVAS_LOGIN:
        data_hora_atual = datetime.datetime.now()
        tempo_bloqueio = settings.MINUTOS_BLOQUEIO_LOGIN

        if data_hora_ultima_tentativa <= \
                   data_hora_atual - datetime.timedelta(minutes=tempo_bloqueio):
            cache.delete(chave_cache)
        else:
            messages.warning(request, u'O tempo de bloqueio do seu usuário '
                                      u'ainda não terminou, tente novamente '
                                      u'mais tarde.')
            form.errors.clear()
            request.method = "GET"

@admin_required
@csrf_protect
def login(request):
    """
    View de login do projeto.

    É utilizada devido a necessidade de verificar a qual site o login se refere.
    Faz cache por usuário do número de tentativas de login.
    """
    dominio = request.get_host()
    chave_cache = request.POST.get('username')
    valor_cache = cache.get(chave_cache)

    form = AutenticacaoForm(request)

    if valor_cache:
        verifica_tentativas_login_cache(request, form, valor_cache, chave_cache)

    if request.method == "POST":
        form = AutenticacaoForm(data=request.POST)

        if form.is_valid():
            usuario = form.get_user()
            url_login_redirect = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Caso seja o usuário que pode acessar as administrações
            # de todos os sites
            if usuario.pk == 1:
                django_login(request, usuario)

                # Verifica se o teste de cookies do navegador funcionou
                if request.session.test_cookie_worked():
                    request.session.delete_test_cookie()

                return HttpResponseRedirect(url_login_redirect)

            # Se o domínio não existir ou o usuário não for responsável pelo
            # site a qual o login acessado faz parte, um erro é exibido na tela
            # de login
            elif not Site.objects.filter(dominio=dominio).exists() or\
               not usuario.cliente.site_set.filter(dominio=dominio).exists():
                form.errors['__all__'] = True
            else:
                django_login(request, usuario)

                if valor_cache:
                    cache.delete(chave_cache)

                # Verifica se o teste de cookies do navegador funcionou
                if request.session.test_cookie_worked():
                    request.session.delete_test_cookie()

                return HttpResponseRedirect(url_login_redirect)

        # Verifica se o erro refere-se ao usuário e senha informados
        elif form.errors.has_key('__all__'):
            if valor_cache:
                verifica_tentativas_login_cache(request, form,
                                                valor_cache, chave_cache)
            else:
                cache.set(chave_cache, [1, datetime.datetime.now()])

    request.session.set_test_cookie()

    try:
        nome_site = Site.objects.get(dominio=dominio).nome
    except:
        raise Http404

    return render_to_response('login.html', {'form':form,
                                             'nome_site':nome_site},
                              context_instance=RequestContext(request)
                        )

@login_required
def logout(request):
    """
    View de Logout do projeto.
    """
    django_logout(request)

    url_login = resolve_url(settings.LOGIN_URL)
    messages.success(request, u'Logout efetuado com sucesso.')

    return HttpResponseRedirect(url_login)

@csrf_protect
def recuperacao_senha(request):
    """
    View de recuperação de senha do projeto.

    Utilizada devido a necessidade de passagem de novos parâmetros ao template
    e ao form.
    """
    dominio = request.get_host()

    try:
        nome_site = Site.objects.get(dominio=dominio).nome
    except:
        raise Http404

    if request.method == "POST":
        form = RecuperacaoSenhaForm(request.POST)
        if form.is_valid():
            url_recuperacao_senha_redirect = \
                    resolve_url(
                        u'/administracao-do-site/recuperacao_senha_email/'
                    )

            opts = {
                'usa_https': request.is_secure(),
                'nome_template_email':
                            u'recuperacao_senha/email_recuperacao_senha.html',
                'nome_template_assunto':
                       u'recuperacao_senha/assunto_email_recuperacao_senha.txt',
                'dominio': dominio,
                'nome_site': nome_site,
            }

            form.save(**opts)

            return HttpResponseRedirect(url_recuperacao_senha_redirect)
    else:
        form = RecuperacaoSenhaForm()

    return TemplateResponse(request,
                            'recuperacao_senha/recuperacao_senha.html',
                            {'form': form,
                             'nome_site': nome_site}
                        )

def recuperacao_senha_concluida(request):
    """
    View de recuperação de senha concluída do projeto. Retorna um template
    informando que as instruções de redefinição de senha foram enviadas
    por e-mail ao usuário.

    Utilizada ao invés da view do django devido a necessidade de passagem dos
    parâmetros 'nome_site' e 'site_url' ao template.
    """
    dominio = request.get_host()

    try:
        nome_site = Site.objects.get(dominio=dominio).nome
    except:
        raise Http404

    return TemplateResponse(request,
                           'recuperacao_senha/recuperacao_senha_concluida.html',
                           {'nome_site': nome_site,
                            'site_url': dominio},
                        )

@sensitive_post_parameters()
@never_cache
def confirmacao_redefinicao_senha(request, uidb36=None, token=None):
    """
    View que verifica o token em um link de recuperação de senha e exibe
    um formulário para a redefinição de senha.

    Sobrescrita para a passagem dos parâmetros 'dominio' e 'nome_site' ao
    template. Também é verificado se o token está sendo passado pelo
    usuário ao site do qual ele é responsável.
    """
    UserModel = get_user_model()
    dominio = request.get_host()

    try:
        nome_site = Site.objects.get(dominio=dominio).nome
    except:
        raise Http404

    assert uidb36 is not None and token is not None

    try:
        uid_int = base36_to_int(uidb36)
        user = UserModel._default_manager.get(pk=uid_int)
    except (ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    # Se o usuário não for responsável pelo site
    if not user.cliente.site_set.filter(dominio=dominio).exists():
        raise Http404

    if user is not None and default_token_generator.check_token(user, token):
        token_valido = True
        if request.method == 'POST':
            form = TrocaSenhaForm(user, request.POST)
            if form.is_valid():
                form.save()
                url_redefinicao_senha_redirect = \
                    resolve_url(
                        u'/administracao-do-site/recuperacao_senha_completa/'
                    )
                return HttpResponseRedirect(url_redefinicao_senha_redirect)
        else:
            form = TrocaSenhaForm(None)
    else:
        token_valido = False
        form = None

    return TemplateResponse(request,
                            'recuperacao_senha/redefinicao_senha.html',
                            {'form': form,
                             'token_valido': token_valido,
                             'site_url': dominio,
                             'nome_site': nome_site}
                        )


def redefinicao_senha_completa(request):
    """
    View de redefinição de senha completa do projeto. Retorna um template
    informando que a senha do usuário for alterada com sucesso.

    Utilizada ao invés da view do django devido a necessidade de passagem dos
    parâmetros 'nome_site' e 'site_url' ao template.
    """
    dominio = request.get_host()

    try:
        nome_site = Site.objects.get(dominio=dominio).nome
    except:
        raise Http404

    return TemplateResponse(request,
                            'recuperacao_senha/redefinicao_senha_completa.html',
                            {'nome_site': nome_site},
                        )

@admin_required
@login_required
def administrar_site(request):
    """
    Página inicial da administração do site, onde é possível escolher
    a página desejada.
    """
    dominio = request.get_host()

    try:
        site = Site.objects.get(dominio=dominio)
        paginas = site.paginas.all()
    except:
        raise Http404

    return render_to_response('administracao_site.html',
                              {'paginas': paginas,
                               'site': site,
                               'pagina_inicial':True},
            context_instance=RequestContext(request))

@admin_required
@login_required
def form_pagina(request, pk_pagina):
    """
    mostra o form para alteração dos dados da página, e as sub-abas
    dos itens específicos daquela página, se houver.
    """
    site, pagina, paginas = get_dados_padrao_views_adm(request, pk_pagina)
    ConteudosForm = get_form_conteudos(pagina)

    # se for post, valida o form e salva os dados
    if request.method == u'POST':
        form = ConteudosForm(request.POST)
        if form.is_valid():
            for i in pagina.conteudos.all():
                i.conteudo = form.cleaned_data[i.nome]
                i.save()
            messages.success(request, u"Conteudo alterado com sucesso!")
            return HttpResponseRedirect('.')
    if request.method == u'GET':
        dados_form = {}
        for i in pagina.conteudos.all().values(u'nome', u'conteudo'):
            dados_form[i[u'nome']] = i[u'conteudo']
        form = ConteudosForm(dados_form)
    abas = {'aba_conteudos': {'form': form}}
    abas['pagina'] = pagina
    abas['paginas'] = paginas
    abas['site'] = site

    return render_to_response('form_pagina.html', abas,
            context_instance=RequestContext(request))

@admin_required
@login_required
def itens_pagina(request, pk_pagina, pk_itempagina):
    """
    Mostra uma listagem daqueles itens daquela página.
    """
    site, pagina, paginas = get_dados_padrao_views_adm(request, pk_pagina)
    item_pagina = pagina.itempagina_set.get(pk=pk_itempagina)

    abas = {}
    abas[u'pagina'] = pagina
    abas[u'item_pagina'] = item_pagina
    abas[u'paginas'] = paginas
    instancias = item_pagina.instanciaitempagina_set.all()
    abas[u'instancias'] = instancias
    abas[u'cabecalho'] = item_pagina.atributoitempagina_set.all()
    abas['site'] = site

    return render_to_response(u'itens_pagina.html', abas,
            context_instance=RequestContext(request))

@admin_required
@login_required
def form_itempagina_novo(request, pk_pagina, pk_itempagina):
    """
    Adição de uma nova instância de um item de uma página.
    """
    site, pagina, paginas = get_dados_padrao_views_adm(request, pk_pagina)
    item_pagina = pagina.itempagina_set.get(pk=pk_itempagina)
    ItemPaginaForm = get_form_item_pagina(item_pagina)

    if request.method == u'POST':
        form = ItemPaginaForm(request.POST, request.FILES)
        if form.is_valid():
            #cria a nova instancia
            nova_instancia = InstanciaItemPagina.objects.\
                                                 create(item_pagina=item_pagina)
            for cab in item_pagina.atributoitempagina_set.all():#loop nos cabeç.
                novo_attr = ValorAtributoInstanciaItemPagina()
                novo_attr.atributo_item_pagina = cab
                novo_attr.instancia_item_pagina = nova_instancia
                novo_attr.valor = trata_valor(novo_attr, form.cleaned_data[cab.identificador])
                novo_attr.save()
            messages.success(request, u"Adicionado com sucesso!")
            return HttpResponseRedirect(
                    u'/administracao-do-site/paginas/%s/itens/%s/' %\
                                         (pagina.pk, item_pagina.pk))

    else:
        form = ItemPaginaForm()
    abas = {}
    abas[u'pagina'] = pagina
    abas[u'item_pagina'] = item_pagina
    abas[u'paginas'] = paginas
    abas[u'form'] = form
    abas['site'] = site

    return render_to_response(u'form_item_pagina.html', abas,
            context_instance=RequestContext(request))

@admin_required
@login_required
def form_itempagina_alterar(request, pk_pagina, pk_itempagina, pk_instancia):
    """
    Alteração de uma instância de um item de uma página.
    """
    site, pagina, paginas = get_dados_padrao_views_adm(request, pk_pagina)
    instancia = InstanciaItemPagina.objects.get(pk=pk_instancia)

    item_pagina = pagina.itempagina_set.get(pk=pk_itempagina)
    ItemPaginaForm = get_form_item_pagina(item_pagina)
    if request.method == u'POST':
        form = ItemPaginaForm(request.POST, request.FILES)
        #validação abaixo para caso não se esteja alterando a imagem
        for k, v in form.fields.items():
            if isinstance(v, ImageField) and k not in request.FILES.keys():
                form.fields.pop(k)
        if form.is_valid():
            #altera os valores da instância existente
            valores = instancia.valoratributoinstanciaitempagina_set.all()
            for val in valores:
                if form.cleaned_data.has_key(val.atributo_item_pagina.identificador):
                    val.valor = trata_valor(val, form.cleaned_data[val.atributo_item_pagina.identificador])
                    val.save()
            messages.success(request, u"Alterado com sucesso!")
            return HttpResponseRedirect(
                    u'/administracao-do-site/paginas/%s/itens/%s/' %\
                                        (pagina.pk, item_pagina.pk))

    else:
        dados_atuais = {}
        dados = instancia.valoratributoinstanciaitempagina_set.all().\
                          values('atributo_item_pagina__identificador',
                                 'atributo_item_pagina__tipo',
                                 'valor')
        for dado in dados:
            valor = dado[u"valor"]
            if valor and dado[u"atributo_item_pagina__tipo"] == 6:#ImageField
                img_atual = File(open(valor))
                img_atual.url = "/static/cli/" + valor.split(settings.ARQUIVOS_UPLOAD_CLIENTES_ROOT)[1]
                valor = img_atual
            dados_atuais[dado['atributo_item_pagina__identificador']] = \
                                                                   valor

        form = ItemPaginaForm(initial=dados_atuais)
    abas = {}
    abas[u'pagina'] = pagina
    abas[u'item_pagina'] = item_pagina
    abas[u'paginas'] = paginas
    abas[u'form'] = form
    abas['site'] = site
    return render_to_response(u'form_item_pagina.html', abas,
            context_instance=RequestContext(request))

@admin_required
@login_required
def itempagina_apagar_instancia(request, pk_pagina, pk_itempagina, pk_instancia):
    """
    Apaga uma instância de um item de uma página.
    """
    site, pagina, paginas = get_dados_padrao_views_adm(request, pk_pagina)
    item_pagina = pagina.itempagina_set.get(pk=pk_itempagina)
    contexto = {u"site": site, u"pagina": pagina, u"paginas": paginas, u"item_pagina": item_pagina}
    if request.method == u'POST':
        if request.POST.get(u"decisao", u"Nao") == u"Sim":
            InstanciaItemPagina.objects.get(pk=pk_instancia).delete()
            messages.success(request, u"Item apagado com sucesso!")
        else:
            messages.success(request, u"O item NAO foi apagado!")
        return HttpResponseRedirect(
                 u'/administracao-do-site/paginas/%s/itens/%s/' %\
                                (pk_pagina, pk_itempagina))
    else:
        return render_to_response(u'confirma_exclusao.html', contexto,
                context_instance=RequestContext(request))

