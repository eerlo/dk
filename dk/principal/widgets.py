#-*- coding: utf-8 -*-
from django.utils.safestring import mark_safe
from django.forms.widgets import FileInput
class AdminImageWidget(FileInput):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            output.append(u' Atualmente: <a href="%s" target="_blank">%s</a><br/>' % \
                (image_url, file_name.split('/')[-1],))
        output.append(super(FileInput, self).render(name, value, attrs))
        return mark_safe(u''.join(output))
