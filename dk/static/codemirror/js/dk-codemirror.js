CodeMirror.fromTextArea(document.getElementById("id_campo_codemirror"), {
    mode: "mode_",
    theme: "theme_",
    lineNumbers: true,
    lineWrapping: true,
    styleActiveLine: true,
    extraKeys: {
        "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },
        "F11": function(cm) {
          cm.setOption("fullScreen", !cm.getOption("fullScreen"));
        },
        "Esc": function(cm) {
          if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
        }
    },
    foldGutter: true,
    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
});
