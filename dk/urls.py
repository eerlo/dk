#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

        url(r'admin/', include(admin.site.urls)),

        url(r'^administracao-do-site/login/', 'dk.principal.views.login'),

        url(r'^administracao-do-site/logout/', 'dk.principal.views.logout'),

        url(r'^administracao-do-site/', include('dk.principal.urls')),

        #URL do redactor
        url(r'^redactor/', include('redactor.urls')),

        url(r'^administracao-do-site/recuperacao_senha/$',
            'dk.principal.views.recuperacao_senha',),

        url(r'^administracao-do-site/recuperacao_senha_email/$',
            'dk.principal.views.recuperacao_senha_concluida',),

        url(r'^administracao-do-site/recuperacao_senha/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
            'dk.principal.views.confirmacao_redefinicao_senha',),

        url(r'^administracao-do-site/recuperacao_senha_completa/$', 
            'dk.principal.views.redefinicao_senha_completa',),

        #TODO esta url deve estar no final para funcionar.
        url(r'^', 'dk.core.views.mostra_pagina_view'),

)

handler404 = 'dk.core.views.erro404'
handler500 = 'dk.core.views.erro500'

